# coding=utf-8

"""
Kakuro exercise.
"""

from z3 import *

def get_kakuro_solution(board, nrows, ncols):
    #
    # Your solution here...
    #
    pass

def draw_board(board, nrows, ncols):
    top = ['┌'] + ['────────────┬' for k in range(ncols-1)] + ['────────────┐'];
    print ''.join(top)
    
    for i in range(nrows):
        print '│',
        for j in range(ncols):
            cell = board[i][j]
            if cell == 'W':
                print '           │',
            elif cell == 'B':
                print '     B     │',
            elif (type(cell) is tuple):
                d,l = cell
                print ' {:>4}\{:<4} │'.format(d,l),
            else: # integer value
                assert type(cell) is int
                print '     {}     │'.format(cell),
        print
        mid = ['├'] + ['────────────┼' for k in range(ncols-1)] + ['────────────┤']
        bottom = ['└'] + ['────────────┴' for k in range(ncols-1)] + ['────────────┘']        
        print ''.join(mid) if i < nrows - 1 else ''.join(bottom)

if __name__ == '__main__':
    #
    # Your tests here...
    #
    pass

    # Example 5x5 Input
    board = [
        ['B', (16,'B'), (12, 'B'), (29, 'B'), 'B'],
        [('B',23), 'W', 'W', 'W', 'B'],
        [('B', 14), 'W', 'W', 'W', (14, 'B')],
        ['B', ('B', 19), 'W', 'W', 'W'],
        ['B', ('B', 15), 'W', 'W', 'W']
    ]

    # # Output:
    # solution = [
    #     ['B', (16, 'B'), (12, 'B'), (29, 'B'), 'B'],
    #     [('B', 23), 9, 6, 8, 'B'],
    #     [('B', 14), 7, 2, 5, (14, 'B')],
    #     ['B', ('B', 19), 3, 7, 9],
    #     ['B', ('B', 15), 1, 9, 5]
    # ]

    # Example 6x5 Input
    board = [
        ['B','B','B',(13, 'B'),(9, 'B')],
        ['B','B',(14,17),'W','W'],
        ['B',(10,12),'W','W','W'],
        [('B',8),'W','W','W','B'],
        [('B',4),'W','W','B','B'],
        [('B',5),'W','W','B','B'],
    ]

    # # Output:
    # solution = [
    #     ['B', 'B', 'B', (13, 'B'), (9, 'B')],
    #     ['B', 'B', (14, 17), 9, 8],
    #     ['B', (10, 12), 8, 3, 1],
    #     [('B', 8), 5, 2, 1, 'B'],
    #     [('B', 4), 3, 1, 'B', 'B'],
    #     [('B', 5), 2, 3, 'B', 'B']
    # ]
