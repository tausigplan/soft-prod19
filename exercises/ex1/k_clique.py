"""
k-clique exercise.
"""

from z3 import *

def get_k_clique(k, V, E):
    #
    # Your solution here...
    #
    pass

def draw_graph(V, E, clique=[], filename='graph', engine='circo'):
    try:
        from graphviz import Graph, Digraph
    except ImportError:
        print "You don't have graphviz python interface installed. Sorry."
        return

    dot = Graph(engine=engine)
    for v in V:
        if v in clique:
            dot.node(str(v), style="filled", fillcolor='red')
        else:
            dot.node(str(v))
    for v1, v2 in E:
        dot.edge(str(v1), str(v2))
    dot.render(filename, cleanup=True, view=True)


if __name__ == '__main__':
    #
    # Your tests here...
    #
    pass
