from z3 import *

# Define boolean variables
p = Bool('p')
q = Bool('q')

# Simple formula 'p => q'
f = Implies(p, q)
s = Solver()
s.add(f)
print s
print s.check()
print s.model()
# Expect ~p, ~q

# Force p to be true
f2 = And(f, p)
s = Solver()
s.add(f2)
print s
print s.check()
print s.model()
# Expect p, q

# Adding formulae creates a big conjunctive formula
s = Solver()
s.add(f)
s.add(p)
print s
print s.check()
print s.model()

# Example of an unsatisfiable formula
s = Solver()
s.add(p)
s.add(Not(p))
print s
print s.check()

# Example of an unsatisfiable formula with an additional variable that is not part of the unsat core
s = Solver()
s.add(p)
s.add(Not(p))
s.add(q)
print s.check()

# Create boolean variable to act as flags (or assumptions) for the different conjuncts
b1, b2, b3 = Bools('b1 b2 b3')

s = Solver()
s.add(Implies(b1,p))
s.add(Implies(b2,Not(p)))
s.add(Implies(b3,q))
print s.check(b1, b2, b3)
print s.unsat_core()
# Expect [b1, b2]

# Example on how to use simplify
print And(p, q, True)
print simplify(And(p, q, True))
print simplify(And(p, False))

# A note on the Z3 type 'True'
s = Solver()
s.add(p)
s.add(Not(q))
print s
print s.check()
m = s.model()
print m

print m[q]
print m[p]

print type(m[p]) is bool

print type(is_true(m[p])) is bool

print m[p] == True
print m[q] == True
print is_true(m[p] == True)
print is_true(m[q] == True)

# Just to drive the point home
x = True 
y = True
print x == y