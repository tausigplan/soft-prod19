#!/bin/bash

###################################
## Simple Example

# Compile to LLVM bitcode
clang -I /home/klee/klee_src/include -emit-llvm -c -g get_sign.c

# Run KLEE
klee get_sign.bc

# List output content
ls klee-last/

# Look at the produced test cases
ktest-tool --write-ints klee-last/test000001.ktest
ktest-tool --write-ints klee-last/test000002.ktest
ktest-tool --write-ints klee-last/test000003.ktest

# Compile the produceds testcases and run them
export LD_LIBRARY_PATH=/home/klee/klee_build/klee/Release+Asserts/lib/:$LD_LIBRARY_PATH
clang -I/home/klee/klee_src/include/ -L/home/klee/klee_build/klee/Release+Asserts/lib/ get_sign.c -lkleeRuntest
KTEST_FILE=klee-last/test000001.ktest ./a.out
echo $?
KTEST_FILE=klee-last/test000002.ktest ./a.out
echo $?
KTEST_FILE=klee-last/test000003.ktest ./a.out
echo $?

##
###################################

###################################
## Maze Example

# Compile and run the maze game
# The solution: ssssddddwwaawwddddssssddwwww
clang maze.c -o maze
./maze

# Now to run KLEE on the maze
# Make sure you annotate the code
clang -I /home/klee/klee_src/include -emit-llvm -c -g maze.c
klee maze.bc

# Look at an example input
ktest-tool --write-ints klee-last/test000184.ktest

# Add assertion to code

# Recompile llvm IR and rerun klee
clang -I /home/klee/klee_src/include -emit-llvm -c -g maze.c
klee maze.bc

# Find error run
ls -l klee-last/ | grep -A2 -B2 err

# Look at the input of the error run
ktest-tool klee-last/test<err-run#>.ktest

# Find all error runs
klee –emit-all-errors maze.bc

# Find the error runs
ls -l klee-last/ | grep -A2 -B2 err

# Look at the inputs of the error runs
ktest-tool klee-last/test<err-run#1>.ktest
ktest-tool klee-last/test<err-run#2>.ktest
ktest-tool klee-last/test<err-run#3>.ktest
ktest-tool klee-last/test<err-run#4>.ktest

clang -I/home/klee/klee_src/include/ -L/home/klee/klee_build/klee/Release+Asserts/lib/  maze.c -lkleeRuntest

##
###################################


