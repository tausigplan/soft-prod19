# coding=utf-8

"""
Soduko exercise.
"""

from z3 import *

def get_soduko_solution(B):
    s = Solver()

    # Cell variables
    board = [[Int('cell_{}_{}'.format(row, col)) for row in range(9)] for col in range(9)]

    # Cell constraints
    for row in range(9):
        for col in range(9):
            s.add(And(board[row][col] > 0, board[row][col] <= 9))
            if (B[row][col] != None):
                s.add(board[row][col] == B[row][col]) 

    # Row constraints
    for row in range(9):
        for col1 in range(9):
            for col2 in range(col1):
                s.add(board[row][col1] != board[row][col2])

    # Column constraints
    for col in range(9):
        for row1 in range(9):
            for row2 in range(row1):
                s.add(board[row1][col] != board[row2][col])

    # # Row constraints
    # for row in range(9):
    #   s.add(Distinct([board[row][col] for col in range(9)]))

    # # Column constraints
    # for col in range(9):
    #   s.add(Distinct([board[row][col] for row in range(9)]))

    # Group constraints
    for grow in range(3):
        for gcol in range(3):
            s.add(Distinct([board[grow * 3 + row][gcol * 3 + col] for row in range(3) for col in range(3)]))

    print "Solver is:"
    print s
    print

    print "Checking SAT"
    res = s.check()
    if res == unsat:
        print "UNSAT, No solution."
        return None
    elif res == unknown:
        print "Unknown ¯\\_(ツ)_/¯"
        return None
    else:
        assert res == sat
        print "SAT, Found solution!"
        m = s.model()
        x =  [[m[board[row][col]] for col in range(9)] for row in range(9)]
        print x
        return x
    
def draw_board(B):
    print '┌───────┬───────┬───────┐'
    
    for i in range(9):
        if (i in [3,6]):
            print '├───────┼───────┼───────┤'
        for j in range(9):
            if (j % 3 == 0) :
                print '│',
            if (B[i][j] == None):
                print ' ',
            else: 
                print B[i][j],
        print '│'
    print '└───────┴───────┴───────┘'

if __name__ == '__main__':

    B = [
        [None, None, None, None, None, None, 6, 8, None],
        [None, None, None, None, 7, 3, None, None, 9],
        [3, None, 9, None, None, None, None, 4, 5],
        [4, 9, None, None, None, None, None, None, None],
        [8, None, 3, None, 5, None, 9, None, 2],
        [None, None, None, None, None, None, None, 3, 6],
        [9, 6, None, None, None, None, 3, None, 8],
        [7, None, None, 6, 8, None, None, None, None],
        [None, 2, 8, None, None, None, None, None, None],
    ]

    draw_board(B)

    solution = get_soduko_solution(B)

    draw_board(solution)
