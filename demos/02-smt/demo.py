from z3 import *

# Integers
I = IntSort()
x, y = Consts('x y', I)

s = Solver()
s.add(x == y + 1 )
print s
print s.check()
print s.model()

s.reset()
s.add(x == y + 1 )
s.add(x == y - 1 )
print s
print s.check()

s.reset()
s.add (x % 3 == y % 3)
s.add (x == y + 6)
print s
print s.check()
print s.model()

s.add(x != 0)
print s
print s.check()
print s.model()

s.reset()

s.add(x % 3 == y % 3)
s.add(x == y + 5)
print s.check()

# Uninterpreted
A    = DeclareSort('A')
a, b = Consts('a b', A)

f = Function('f', A, A)

s.reset()
s.add(f(a) != f(b))
print s.check()
print s.model()
print s.model().evaluate(f(a))
print s.model().evaluate(f(b))

print s.model().get_universe(A) # get all elements of sort A in the universe of the model

s.reset()

s.add(f(a) == f(b))
print s.check()
print s.model()
print s.model().evaluate(f(a))
print s.model().evaluate(f(b))

s.reset() 
P = Function('P', A, BoolSort())
s.add(P(a))
s.add(Not(P(b)))
print s.check()
print s.model()

# Combined 
s.reset()
g = Function('g', A, I)
s.add(Implies(P(a), g(a) == g(b) + 10))

print s.check()
print s.model()
print s.model().evaluate(g(a))
print s.model().evaluate(g(b))

s.add(P(a))

print s.check()
print s.model()
print s.model().evaluate(g(a))
print s.model().evaluate(g(b))

s.add(Implies(P(b), g(b) == 11))
s.add(P(b))

print s.check()
print s.model()
print s.model().evaluate(g(a))
print s.model().evaluate(g(b))
